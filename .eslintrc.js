module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'plugin:vue/vue3-recommended',
    'plugin:vuetify/base',
  ],
  // add your custom rules here
  rules: {
    'vue/multi-word-component-names': 0,
  },
}
