import { defineStore } from 'pinia'
import { apiGetCurrentCertification } from '~/api/certifications'

export const useCertificationsStore = defineStore('certifications', {
  state: () => {
    return {
      certification: null,
    }
  },
  actions: {
    UPDATE_CERT(cert) {
      this.certification = cert
    },
    getCertification() {
      return new Promise((resolve, reject) => {
        apiGetCurrentCertification()
          .then((response) => {
            this.UPDATE_CERT(response)
            resolve(response)
          })
          .catch((error) => {
            this.reset()
            reject(error)
          })
      })
    },
    reset() {
      this.$reset()
    },
  },
})
