import { defineStore } from 'pinia'
import { apiGetQuestionsForCategory, apiPostResponses } from '~/api/questions'
import { useCategoriesStore } from '~/store/categories'

export const useQuestionsStore = defineStore('questions', {
  state: () => {
    return {
      currentQuestion: null,
      questions: [],
      // answers should be per category
      answers: {},
      // result should be per category
      result: {},
    }
  },
  actions: {
    //= ===== former mutations
    UPDATE_QUESTIONS(questions) {
      this.questions = questions
    },
    UPDATE_RESULT(result) {
      const categoriesStore = useCategoriesStore()
      const currentCategory = categoriesStore.currentCategory

      this.result[currentCategory] = result
    },
    ADD_ANSWER(answer) {
      const response = { ...this.currentQuestion }
      response.answer = answer
      const categoriesStore = useCategoriesStore()
      const currentCategory = categoriesStore.currentCategory
      if (!this.answers[currentCategory]) {
        this.answers[currentCategory] = []
      }
      this.answers[currentCategory] = [
        ...this.answers[currentCategory],
        response,
      ]
    },
    UPDATE_CURRENT_QUESTION(questionId) {
      this.currentQuestion = this.questions[questionId]
    },
    RESET_CURRENT_QUESTION() {
      this.currentQuestion = null
    },
    //= ===== API actions
    getQuestions(category) {
      return new Promise((resolve, reject) => {
        apiGetQuestionsForCategory({
          category,
        })
          .then((response) => {
            this.UPDATE_QUESTIONS(response)
            if (response.length) {
              this.UPDATE_CURRENT_QUESTION(0)
            }
            resolve(response)
          })
          .catch((error) => {
            this.reset()
            reject(error)
          })
      })
    },
    postAnswers(currentCategory) {
      const params = {
        answers: this.answers[currentCategory],
      }

      return new Promise((resolve, reject) => {
        apiPostResponses(params)
          .then((response) => {
            this.resetQuestions()
            this.UPDATE_RESULT(response)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    answerQuestion(answer) {
      return new Promise((resolve, reject) => {
        this.ADD_ANSWER(answer)
        this.progressToNextQuestion(answer)
      })
    },
    progressToNextQuestion(answer) {
      return new Promise(() => {
        let nextQuestionId = +this.currentQuestion.follow_up[answer]
        let hasNextQuestion = this.questions.some(
          (question) => +question.id === nextQuestionId,
        )
        if (hasNextQuestion) {
          this.UPDATE_CURRENT_QUESTION(nextQuestionId)
        } else {
          // what happens if there is no next question?

          // (unfriendly scenario, tbc)
          // no more current question
          // display a message showing that the user is not compliant with the standard
          // prompt the user to review answers and submit them
          // in response get a list of recommendations

          // or

          // ask the next question in line
          // when the user reaches no more lines, or 'proceed to next section', delete the current
          // question
          // prompt the user to review answers and sumbit them
          // in response get a list of recommendations

          // ask the next question in line
          nextQuestionId = this.currentQuestion.id + 1
          hasNextQuestion = this.questions.some(
            (question) => +question.id === nextQuestionId,
          )
          if (hasNextQuestion) {
            this.UPDATE_CURRENT_QUESTION(nextQuestionId)
          } else {
            // user reached the end of the questionnaire
            // ask them to review their answers and submit
            this.RESET_CURRENT_QUESTION()
          }
        }
      })
    },

    reset() {
      this.$reset()
    },
    resetQuestions() {
      this.currentQuestion = null
      this.questions = []
    },
  },
})
