import { defineStore } from 'pinia'
import { apiGetCategories } from '~/api/categories'

export const useCategoriesStore = defineStore('categories', {
  state: () => {
    return {
      categories: [],
      currentCategory: null,
    }
  },
  actions: {
    UPDATE_CATEGORIES(categories) {
      this.categories = categories
    },
    UPDATE_CURRENT_CATEGORY(category) {
      this.currentCategory = category
    },
    // former actions
    reset() {
      this.$reset()
    },
    getCategories() {
      return new Promise((resolve, reject) => {
        apiGetCategories()
          .then((response) => {
            this.UPDATE_CATEGORIES(response)
            resolve(response)
          })
          .catch((error) => {
            this.reset()
            reject(error)
          })
      })
    },
    setCurrentCategory(category) {
      if (this.categories.includes(category)) {
        this.UPDATE_CURRENT_CATEGORY(category)
      }
    },
  },
})
