## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npx nuxi dev

# build for production and launch server
$ npx nuxi build
$ npx nuxi start
```

The project uses the [Vuetify Nuxt module](https://vuetify-nuxt-module.netlify.app/). It runs on 
Vue 3, Nuxt 3, Pinia, and Vuetify.
