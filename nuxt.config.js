import { defineNuxtConfig } from 'nuxt/config'

export default defineNuxtConfig({
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
  modules: ['@pinia/nuxt', 'vuetify-nuxt-module'],
  vuetify: {
    moduleOptions: {
      /* module specific options */
    },
    vuetifyOptions: {
      /* vuetify options */
    },
  },
})
