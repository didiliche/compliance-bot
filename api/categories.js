import { categories } from '~/api/fixtures/categories'

const delay = 300

export function apiGetCategories(params) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(categories)
    }, delay)
  })
}
