import { questions } from '~/api/fixtures/questions'

const delay = 300

export function apiGetQuestionsForCategory(params) {
  const questionsForCategory = questions
  // const category = params.category
  // const questionsForCategory = questions[category]
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(questionsForCategory)
    }, delay)
  })
}
export function apiPostResponses(params) {
  const answers = params.answers

  const recommendations = []
  for (const answer of answers) {
    // reached the end and there are no recommendations
    if (
      answer.follow_up[answer.answer] === 'Proceed to next section' &&
      !recommendations.length
    ) {
      recommendations.push('Proceed to next section')
      break
    }
    // ignore "proceed to next section" until we reach the end
    else if (answer.follow_up[answer.answer] === 'Proceed to next section') {
      continue
    }
    // add recommendation when the question does not lead to another one
    else if (typeof answer.follow_up[answer.answer] !== 'number') {
      recommendations.push(answer.follow_up[answer.answer])
    }
  }

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(recommendations)
    }, delay)
  })
}
