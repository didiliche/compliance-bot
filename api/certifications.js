import { certifications } from '~/api/fixtures/certifications'

const delay = 300

export function apiGetCurrentCertification() {
  const currentCertification = certifications[0]
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(currentCertification)
    }, delay)
  })
}
