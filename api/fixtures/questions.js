export const questions = [
  {
    id: 1,
    tags: ['AWS_EC2_Instances', 'Scope'],
    question: 'Is the scope of AWS EC2 instances clearly defined?',
    follow_up: {
      Yes: 2,
      No: 'Review_and_clarify_the_scope',
    },
  },
  {
    id: 2,
    tags: ['AWS_EC2_Instances', 'Encryption'],
    question:
      'Are encryption mechanisms in place for protecting data on AWS EC2 instances?',
    follow_up: {
      Yes: 3,
      No: 'Investigate_encryption_measures',
    },
  },
  {
    id: 3,
    tags: ['AWS_EC2_Instances', 'Access_Control'],
    question:
      'Are access controls implemented to restrict access to AWS EC2 instances?',
    follow_up: {
      Yes: 4,
      No: 'Examine_access_control_measures',
    },
  },
  {
    id: 4,
    tags: ['Amazon_RDS_Database', 'Scope'],
    question: 'Is the scope of the Amazon RDS database clearly defined?',
    follow_up: {
      Yes: 5,
      No: 'Review_and_clarify_the_scope',
    },
  },
  {
    id: 5,
    tags: ['Amazon_RDS_Database', 'Encryption'],
    question:
      'Is encryption in place for protecting data in the Amazon RDS database?',
    follow_up: {
      Yes: 6,
      No: 'Investigate_encryption_measures',
    },
  },
  {
    id: 6,
    tags: ['Amazon_RDS_Database', 'Access_Control'],
    question: 'Are access controls implemented for the Amazon RDS database?',
    follow_up: {
      Yes: 7,
      No: 'Examine_access_control_measures',
    },
  },
  {
    id: 7,
    tags: ['Network_Security', 'Scope'],
    question:
      'Is the AWS VPC clearly defined for the cardholder data environment?',
    follow_up: {
      Yes: 8,
      No: 'Review_and_clarify_the_AWS_VPC',
    },
  },
  {
    id: 8,
    tags: ['Network_Security', 'Network_Segmentation'],
    question:
      'Are network segments effectively isolating cardholder data from other systems?',
    follow_up: {
      Yes: 9,
      No: 'Evaluate_network_segmentation',
    },
  },
  {
    id: 9,
    tags: ['Network_Security', 'Security_Groups'],
    question:
      'Are security groups configured to restrict access within the VPC?',
    follow_up: {
      Yes: 10,
      No: 'Examine_security_group_configurations',
    },
  },
  {
    id: 10,
    tags: ['Vulnerability_Management', 'Vulnerability_Assessment'],
    question:
      'Are regular vulnerability assessments and penetration tests conducted for AWS EC2 instances?',
    follow_up: {
      Yes: 11,
      No: 'Assess_vulnerability_management_practices',
    },
  },
  {
    id: 11,
    tags: ['Security_Policies_and_Procedures', 'Documentation'],
    question:
      'Is there documentation for security policies and procedures related to PCI DSS compliance in the AWS environment?',
    follow_up: {
      Yes: 12,
      No: 'Request_documentation',
    },
  },
  {
    id: 12,
    tags: ['Incident_Response', 'Incident_Response_Plan'],
    question:
      'Is there an incident response plan in place, and is it configured for AWS Config Rules and CloudWatch Alarms?',
    follow_up: {
      Yes: 13,
      No: 'Evaluate_incident_response_readiness',
    },
  },
  {
    id: 13,
    tags: ['Physical_Security', 'Physical_Access'],
    question:
      'Is physical access to AWS data centers restricted and monitored?',
    follow_up: {
      Yes: 14,
      No: 'Examine_physical_security_measures',
    },
  },
  {
    id: 14,
    tags: ['Logging_and_Monitoring', 'CloudWatch_Logs'],
    question:
      'Are AWS CloudWatch Logs configured for AWS EC2 instances and the Amazon RDS database?',
    follow_up: {
      Yes: 15,
      No: 'Configure_AWS_CloudWatch_Logs',
    },
  },
  {
    id: 15,
    tags: ['Logging_and_Monitoring', 'Monitoring_System'],
    question: 'Is there a monitoring system in place using AWS CloudWatch?',
    follow_up: {
      Yes: 16,
      No: 'Implement_a_monitoring_system',
    },
  },
  {
    id: 16,
    tags: ['Third_Party_Compliance', 'Third_Party_Integration'],
    question:
      'Are measures in place to ensure third-party compliance with PCI DSS, especially for services integrated into the AWS environment?',
    follow_up: {
      Yes: 17,
      No: 'Assess_third_party_compliance_measures',
    },
  },
  {
    id: 17,
    tags: ['Change_Management', 'CloudFormation'],
    question:
      'Is AWS CloudFormation used for change management, especially for systems processing cardholder data?',
    follow_up: {
      Yes: 18,
      No: 'Evaluate_change_management_practices',
    },
  },
  {
    id: 18,
    tags: ['AWS_EC2_Instances', 'Scope'],
    question:
      'Are specific roles assigned to individuals or processes within the scope of AWS EC2 instances?',
    follow_up: {
      Yes: 19,
      No: 'Assign_roles_within_the_scope',
    },
  },
  {
    id: 19,
    tags: ['AWS_EC2_Instances', 'Encryption'],
    question:
      'Is data-at-rest encryption configured for sensitive data on AWS EC2 instances?',
    follow_up: {
      Yes: 20,
      No: 'Configure_data-at-rest_encryption',
    },
  },
  {
    id: 20,
    tags: ['AWS_EC2_Instances', 'Access_Control'],
    question:
      'Are multi-factor authentication (MFA) mechanisms implemented for access to AWS EC2 instances?',
    follow_up: {
      Yes: 40,
      No: 'Implement_MFA_for_access',
    },
  },
  {
    id: 21,
    tags: ['Amazon_RDS_Database', 'Scope'],
    question:
      'Are specific roles assigned to individuals or processes within the scope of the Amazon RDS database?',
    follow_up: {
      Yes: 22,
      No: 'Assign_roles_within_the_scope',
    },
  },
  {
    id: 22,
    tags: ['Amazon_RDS_Database', 'Encryption'],
    question:
      'Is data-at-rest encryption configured for sensitive data in the Amazon RDS database?',
    follow_up: {
      Yes: 23,
      No: 'Configure_data-at-rest_encryption',
    },
  },
  {
    id: 23,
    tags: ['Amazon_RDS_Database', 'Access_Control'],
    question:
      'Are multi-factor authentication (MFA) mechanisms implemented for access to the Amazon RDS database?',
    follow_up: {
      Yes: 40,
      No: 'Implement_MFA_for_access',
    },
  },
  {
    id: 24,
    tags: ['Network_Security', 'Scope'],
    question:
      'Are network diagrams and documentation updated regularly for the AWS VPC?',
    follow_up: {
      Yes: 25,
      No: 'Update_network_diagrams_and_documentation',
    },
  },
  {
    id: 25,
    tags: ['Network_Security', 'Network_Segmentation'],
    question:
      'Is traffic between network segments monitored and logged within the AWS VPC?',
    follow_up: {
      Yes: 26,
      No: 'Implement_traffic_monitoring',
    },
  },
  {
    id: 26,
    tags: ['Network_Security', 'Security_Groups'],
    question:
      'Are security groups regularly reviewed and updated within the AWS VPC?',
    follow_up: {
      Yes: 40,
      No: 'Review_and_update_security_groups',
    },
  },
  {
    id: 27,
    tags: ['Vulnerability_Management', 'Vulnerability_Assessment'],
    question:
      'Are vulnerability assessments conducted using both automated tools and manual testing for AWS EC2 instances?',
    follow_up: {
      Yes: 28,
      No: 'Enhance_vulnerability_assessment_approach',
    },
  },
  {
    id: 28,
    tags: ['Vulnerability_Management', 'Patch_Management'],
    question:
      'Is a robust patch management process in place for addressing vulnerabilities identified in AWS EC2 instances?',
    follow_up: {
      Yes: 29,
      No: 'Establish_patch_management_process',
    },
  },
  {
    id: 29,
    tags: ['Vulnerability_Management', 'Penetration_Testing'],
    question:
      'Is penetration testing conducted periodically to identify and address security weaknesses in AWS EC2 instances?',
    follow_up: {
      Yes: 40,
      No: 'Incorporate_penetration_testing',
    },
  },
  {
    id: 30,
    tags: ['Security_Policies_and_Procedures', 'Documentation'],
    question:
      'Are security policies and procedures regularly reviewed and updated to align with changes in the AWS environment?',
    follow_up: {
      Yes: 31,
      No: 'Review_and_update_security_policies',
    },
  },
  {
    id: 31,
    tags: ['Security_Policies_and_Procedures', 'Training'],
    question:
      'Is training provided to personnel regarding changes in security policies and procedures specific to PCI DSS compliance in the AWS environment?',
    follow_up: {
      Yes: 32,
      No: 'Implement_training_program',
    },
  },
  {
    id: 32,
    tags: ['Security_Policies_and_Procedures', 'Incident_Response'],
    question:
      'Are incident response procedures integrated into overall security policies for the AWS environment?',
    follow_up: {
      Yes: 40,
      No: 'Integrate_incident_response_procedures',
    },
  },
  {
    id: 33,
    tags: ['Incident_Response', 'Incident_Response_Plan'],
    question:
      'Is the incident response plan tested regularly through simulated scenarios for AWS Config Rules and CloudWatch Alarms?',
    follow_up: {
      Yes: 34,
      No: 'Initiate_incident_response_plan_testing',
    },
  },
  {
    id: 34,
    tags: ['Incident_Response', 'Communication'],
    question:
      'Is there a communication plan in place to notify relevant stakeholders during security incidents in the AWS environment?',
    follow_up: {
      Yes: 35,
      No: 'Establish_communication_plan',
    },
  },
  {
    id: 35,
    tags: ['Incident_Response', 'Documentation'],
    question:
      'Are incident response actions and outcomes documented for analysis and improvement in the AWS environment?',
    follow_up: {
      Yes: 40,
      No: 'Document_incident_response_actions',
    },
  },
  {
    id: 36,
    tags: ['Physical_Security', 'Physical_Access'],
    question:
      'Are physical access controls for AWS data centers reviewed and tested periodically?',
    follow_up: {
      Yes: 37,
      No: 'Enhance_physical_access_controls_review',
    },
  },
  {
    id: 37,
    tags: ['Physical_Security', 'Monitoring'],
    question:
      'Is physical access to AWS data centers monitored through surveillance and access logs?',
    follow_up: {
      Yes: 38,
      No: 'Implement_physical_access_monitoring',
    },
  },
  {
    id: 38,
    tags: ['Physical_Security', 'Incident_Response'],
    question:
      'Is there an incident response plan specifically addressing physical security incidents in AWS data centers?',
    follow_up: {
      Yes: 40,
      No: 'Develop_physical_security_incident_response_plan',
    },
  },
  {
    id: 39,
    tags: ['Logging_and_Monitoring', 'CloudWatch_Logs'],
    question:
      'Are AWS CloudWatch Logs configured to capture relevant logs for AWS EC2 instances and the Amazon RDS database?',
    follow_up: {
      Yes: 40,
      No: 'Configure_AWS_CloudWatch_Logs',
    },
  },
  {
    id: 40,
    tags: ['Logging_and_Monitoring', 'Log_Retention'],
    question:
      'Is there a log retention policy in place to retain logs for a sufficient period within AWS CloudWatch?',
    follow_up: {
      Yes: 41,
      No: 'Establish_log_retention_policy',
    },
  },
  {
    id: 41,
    tags: ['Logging_and_Monitoring', 'Log_Analysis'],
    question:
      'Are logs regularly analyzed for security events and anomalies within AWS CloudWatch?',
    follow_up: {
      Yes: 42,
      No: 'Enhance_log_analysis_process',
    },
  },
  {
    id: 42,
    tags: ['Logging_and_Monitoring', 'Monitoring_System'],
    question:
      'Is the monitoring system using AWS CloudWatch configured to provide real-time alerts for critical events?',
    follow_up: {
      Yes: 43,
      No: 'Configure_real-time_alerts',
    },
  },
  {
    id: 43,
    tags: ['Logging_and_Monitoring', 'Incident_Response'],
    question:
      'Is the monitoring system integrated with the incident response plan for swift action in the event of security incidents?',
    follow_up: {
      Yes: 44,
      No: 'Integrate_monitoring_with_incident_response',
    },
  },
  {
    id: 44,
    tags: ['Logging_and_Monitoring', 'Documentation'],
    question:
      'Are monitoring configurations and outcomes documented for analysis and improvement in the AWS environment?',
    follow_up: {
      Yes: 45,
      No: 'Document_monitoring_configurations',
    },
  },
  {
    id: 45,
    tags: ['Third_Party_Compliance', 'Assessment'],
    question:
      'Is there a regular assessment process to evaluate third-party compliance with PCI DSS requirements in the AWS environment?',
    follow_up: {
      Yes: 46,
      No: 'Establish_third_party_compliance_assessment',
    },
  },
  {
    id: 46,
    tags: ['Third_Party_Compliance', 'Documentation'],
    question:
      'Are documentation and evidence obtained from third parties regarding PCI DSS compliance measures integrated into AWS compliance records?',
    follow_up: {
      Yes: 47,
      No: 'Integrate_third_party_compliance_documentation',
    },
  },
  {
    id: 47,
    tags: ['Third_Party_Compliance', 'Incident_Response'],
    question:
      'Is there an incident response plan specific to incidents involving third-party services in the AWS environment?',
    follow_up: {
      Yes: 48,
      No: 'Develop_third_party_incident_response_plan',
    },
  },
  {
    id: 48,
    tags: ['Change_Management', 'CloudFormation'],
    question:
      'Is AWS CloudFormation used for tracking changes in configurations and infrastructure for systems processing cardholder data?',
    follow_up: {
      Yes: 49,
      No: 'Implement_AWS_CloudFormation_for_tracking_changes',
    },
  },
  {
    id: 49,
    tags: ['Change_Management', 'Documentation'],
    question:
      'Is there documentation for changes made using AWS CloudFormation in the AWS environment?',
    follow_up: {
      Yes: 50,
      No: 'Establish_documentation_for_AWS_CloudFormation_changes',
    },
  },
  {
    id: 50,
    tags: ['Change_Management', 'Approval_Process'],
    question:
      'Is there an approval process in place for changes made using AWS CloudFormation?',
    follow_up: {
      Yes: 51,
      No: 'Implement_approval_process_for_AWS_CloudFormation_changes',
    },
  },
  {
    id: 51,
    tags: ['Application_Security', 'Code_Review'],
    question:
      'Is there a systematic code review process in place to identify and mitigate security vulnerabilities in applications?',
    follow_up: {
      Yes: 52,
      No: 'Establish_code_review_process',
    },
  },
  {
    id: 52,
    tags: ['Application_Security', 'Static_Analysis'],
    question:
      'Are static code analysis tools utilized to scan applications for security vulnerabilities during the development process?',
    follow_up: {
      Yes: 53,
      No: 'Integrate_static_code_analysis_tools',
    },
  },
  {
    id: 53,
    tags: ['Application_Security', 'Dynamic_Analysis'],
    question:
      'Is dynamic application security testing (DAST) performed to identify and address runtime vulnerabilities?',
    follow_up: {
      Yes: 54,
      No: 'Implement_dynamic_application_security_testing',
    },
  },
  {
    id: 54,
    tags: ['Application_Security', 'Dependency_Scanning'],
    question:
      'Is there a process to regularly scan and monitor third-party dependencies for known security vulnerabilities?',
    follow_up: {
      Yes: 55,
      No: 'Incorporate_dependency_scanning',
    },
  },
  {
    id: 55,
    tags: ['Application_Security', 'Authentication'],
    question:
      'Are strong authentication mechanisms, such as multi-factor authentication (MFA), implemented for application access?',
    follow_up: {
      Yes: 56,
      No: 'Implement_strong_authentication',
    },
  },
  {
    id: 56,
    tags: ['Application_Security', 'Session_Management'],
    question:
      'Is session management implemented securely to prevent session hijacking and ensure proper session timeout?',
    follow_up: {
      Yes: 57,
      No: 'Enhance_session_management',
    },
  },
  {
    id: 57,
    tags: ['Application_Security', 'Authorization'],
    question:
      'Is there a robust authorization mechanism in place to ensure users have the appropriate access permissions?',
    follow_up: {
      Yes: 58,
      No: 'Improve_authorization_mechanism',
    },
  },
  {
    id: 58,
    tags: ['Application_Security', 'Input_Validation'],
    question:
      'Is input validation implemented thoroughly to prevent common security risks such as SQL injection and cross-site scripting (XSS)?',
    follow_up: {
      Yes: 59,
      No: 'Strengthen_input_validation',
    },
  },
  {
    id: 59,
    tags: ['Application_Security', 'Security_Headers'],
    question:
      'Are security headers, such as Content Security Policy (CSP), implemented to enhance protection against various web-based attacks?',
    follow_up: {
      Yes: 60,
      No: 'Implement_security_headers',
    },
  },
  {
    id: 60,
    tags: ['Application_Security', 'Error_Handling'],
    question:
      'Is error handling implemented securely to avoid exposing sensitive information and provide meaningful error messages to users?',
    follow_up: {
      Yes: 61,
      No: 'Enhance_error_handling',
    },
  },
  {
    id: 61,
    tags: ['Encryption_Mechanisms', 'Algorithm_Selection'],
    question:
      'Are industry-standard cryptographic algorithms selected for data encryption, and is the selection process documented?',
    follow_up: {
      Yes: 62,
      No: 'Review_and_document_algorithm_selection',
    },
  },
  {
    id: 62,
    tags: ['Encryption_Mechanisms', 'Key_Management'],
    question:
      'Is there a secure key management process in place for generating, storing, and rotating encryption keys?',
    follow_up: {
      Yes: 63,
      No: 'Implement_secure_key_management',
    },
  },
  {
    id: 63,
    tags: ['Encryption_Mechanisms', 'Data_At_Rest'],
    question:
      'Is data-at-rest encryption implemented, and are sensitive data files and databases protected using encryption?',
    follow_up: {
      Yes: 64,
      No: 'Configure_data-at-rest_encryption',
    },
  },
  {
    id: 64,
    tags: ['Encryption_Mechanisms', 'Data_In_Transit'],
    question:
      'Is encryption applied to data transmitted between components and external systems, especially over public networks?',
    follow_up: {
      Yes: 65,
      No: 'Secure_data_in_transit',
    },
  },
  {
    id: 65,
    tags: ['Encryption_Mechanisms', 'Certificate_Management'],
    question:
      'Is there a robust certificate management process for handling SSL/TLS certificates and ensuring their validity and security?',
    follow_up: {
      Yes: 66,
      No: 'Enhance_certificate_management',
    },
  },
  {
    id: 66,
    tags: ['Encryption_Mechanisms', 'Hardware_Security_Modules'],
    question:
      'Are Hardware Security Modules (HSMs) used to enhance the security of cryptographic operations and key storage?',
    follow_up: {
      Yes: 67,
      No: 'Consider_implementation_of_HSMs',
    },
  },
  {
    id: 67,
    tags: ['Encryption_Mechanisms', 'HSM_Implementation'],
    question:
      'If HSMs are used, is their implementation configured securely, and are they regularly tested for proper functionality?',
    follow_up: {
      Yes: 68,
      No: 'Review_and_optimize_HSM_implementation',
    },
  },
  {
    id: 68,
    tags: ['Encryption_Mechanisms', 'Algorithm_Auditing'],
    question:
      'Is there a periodic audit conducted to assess the security and compliance of cryptographic algorithms used in the application?',
    follow_up: {
      Yes: 69,
      No: 'Initiate_algorithm_audit',
    },
  },
  {
    id: 69,
    tags: ['Authentication_Mechanisms', 'Biometric_Authentication'],
    question:
      'Is biometric authentication implemented, and are the biometric templates securely stored and processed?',
    follow_up: {
      Yes: 70,
      No: 'Evaluate_biometric_authentication_implementation',
    },
  },
  {
    id: 70,
    tags: ['Authentication_Mechanisms', 'Tokenization'],
    question:
      'Is tokenization used as a method to enhance the security of sensitive data, especially during payment transactions?',
    follow_up: {
      Yes: 71,
      No: 'Consider_tokenization_implementation',
    },
  },
  {
    id: 71,
    tags: ['Authentication_Mechanisms', 'Adaptive_Authentication'],
    question:
      'Is adaptive authentication implemented to dynamically adjust authentication requirements based on risk factors and user behavior?',
    follow_up: {
      Yes: 72,
      No: 'Explore_adaptive_authentication',
    },
  },
  {
    id: 72,
    tags: ['Description_of_Payment_Card_Business', 'Cardholder_Data_Storage'],
    question:
      'Have you specified how and in what capacity does your business store, process, and/or' +
      ' transmit cardholder data?',
    follow_up: {
      Yes: 73,
      No: 'Specify_cardholder_data_handling',
    },
  },
  {
    id: 73,
    tags: ['Locations', 'Facility_Types'],
    question:
      'Have you specified the types of facilities and a summary of locations included in the' +
      ' PCI DSS review.',
    follow_up: {
      Yes: 74,
      No: 'Specify_facility_locations',
    },
  },
  {
    id: 74,
    tags: ['Payment_Application', 'Use_of_Payment_Applications'],
    question: 'Does the organization use one or more Payment Applications?',
    follow_up: {
      Yes: 75,
      No: 'No_Payment_Applications_used',
    },
  },
  {
    id: 75,
    tags: [
      'Payment_Application_Details',
      'Provide_Payment_Application_Information',
    ],
    question:
      'Provide information regarding the Payment Applications your organization uses.',
    follow_up: {
      Yes: 'Proceed to next section',
      No: 'No_Payment_Applications_used',
    },
  },
  {
    id: 76,
    tags: ['Description_of_Environment', 'High_Level_Description'],
    question:
      'Have you specified a high-level description of the environment covered by this assessment.',
    follow_up: {
      Yes: 77,
      No: 'Specify_environment_description',
    },
  },
  {
    id: 77,
    tags: ['Description_of_Environment', 'Network_Segmentation'],
    question:
      'Does your business use network segmentation to affect the scope of your PCI DSS environment?',
    follow_up: {
      Yes: 78,
      No: 'No_network_segmentation_used',
    },
  },
  {
    id: 78,
    tags: ['Description_of_Environment', 'Third_Party_Sharing'],
    question:
      'Does your company share cardholder data with any third-party service providers?',
    follow_up: {
      Yes: 79,
      No: 'No_cardholder_data_shared_with_third_parties',
    },
  },
  {
    id: 79,
    tags: ['Description_of_Environment', 'Third_Party_Details'],
    question:
      'If Yes, provide the name and description of the service provider.',
    follow_up: {
      Yes: 'Proceed to next section',
      No: 'No_cardholder_data_shared_with_third_parties',
    },
  },
  {
    id: 80,
    tags: ['Eligibility_to_Complete_SAQ_A', 'Certification_Eligibility'],
    question:
      'Merchant certifies eligibility to complete SAQ A based on the payment channel.',
    follow_up: {
      Yes: 'Proceed to SAQ A',
      No: 'Not_eligible_for_SAQ_A',
    },
  },
  {
    id: 81,
    tags: ['SAQ_A_Requirement_9', 'Physical_Access_Restriction'],
    question:
      'Are all media physically secured, including computers, removable electronic media, paper receipts, and faxes?',
    follow_up: {
      Yes: 82,
      No: 'Implement_physical_security_measures',
    },
  },
  {
    id: 82,
    tags: ['SAQ_A_Requirement_9', 'Media_Distribution_Control'],
    question:
      'Is strict control maintained over the internal or external distribution of any kind of media?',
    follow_up: {
      Yes: 83,
      No: 'Implement_media_distribution_controls',
    },
  },
  {
    id: 83,
    tags: ['SAQ_A_Requirement_9', 'Media_Classification'],
    question:
      'Is media classified so the sensitivity of the data can be determined?',
    follow_up: {
      Yes: 84,
      No: 'Establish_media_classification_process',
    },
  },
  {
    id: 84,
    tags: ['SAQ_A_Requirement_9', 'Media_Security_Tracking'],
    question:
      'Is media sent by secured courier or other delivery method that can be accurately tracked?',
    follow_up: {
      Yes: 85,
      No: 'Secure_media_delivery_methods',
    },
  },
  {
    id: 85,
    tags: ['SAQ_A_Requirement_9', 'Media_Distribution_Approval'],
    question: 'Is management approval obtained prior to moving the media?',
    follow_up: {
      Yes: 86,
      No: 'Establish_media_distribution_approval_process',
    },
  },
  {
    id: 86,
    tags: ['SAQ_A_Requirement_9', 'Media_Storage_Access_Control'],
    question:
      'Is strict control maintained over the storage and accessibility of media?',
    follow_up: {
      Yes: 87,
      No: 'Enhance_media_storage_access_controls',
    },
  },
  {
    id: 87,
    tags: ['SAQ_A_Requirement_9', 'Media_Destruction_Policies'],
    question:
      'Is all media destroyed when it is no longer needed for business or legal reasons?',
    follow_up: {
      Yes: 88,
      No: 'Implement_media_destruction_policies',
    },
  },
  {
    id: 88,
    tags: ['SAQ_A_Requirement_9', 'Hardcopy_Media_Destruction'],
    question:
      'Are hardcopy materials cross-cut shredded, incinerated, or pulped so that cardholder data cannot be reconstructed?',
    follow_up: {
      Yes: 89,
      No: 'Enhance_hardcopy_media_destruction_methods',
    },
  },
  {
    id: 89,
    tags: ['SAQ_A_Requirement_9', 'Secure_Container_Usage'],
    question:
      'Are storage containers used for materials that contain information to be destroyed secured to prevent access to the contents?',
    follow_up: {
      Yes: 90,
      No: 'Implement_secure_storage_container_usage',
    },
  },
  {
    id: 90,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_Management'],
    question:
      'Are policies and procedures maintained and implemented to manage service providers with whom cardholder data is shared or that could affect the security of cardholder data?',
    follow_up: {
      Yes: 91,
      No: 'Establish_service_provider_management_policies',
    },
  },
  {
    id: 91,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_List_Maintenance'],
    question: 'Is a list of service providers maintained?',
    follow_up: {
      Yes: 92,
      No: 'Maintain_service_provider_list',
    },
  },
  {
    id: 92,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_Agreement_Maintenance'],
    question:
      'Is a written agreement maintained that includes an acknowledgement that the service providers are responsible for the security of cardholder data?',
    follow_up: {
      Yes: 93,
      No: 'Maintain_service_provider_agreements',
    },
  },
  {
    id: 93,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_Engagement_Process'],
    question:
      'Is there an established process for engaging service providers, including proper due diligence prior to engagement?',
    follow_up: {
      Yes: 94,
      No: 'Establish_service_provider_engagement_process',
    },
  },
  {
    id: 94,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_Compliance_Monitoring'],
    question:
      'Is a program maintained to monitor service providers’ PCI DSS compliance status at least annually?',
    follow_up: {
      Yes: 95,
      No: 'Implement_service_provider_compliance_monitoring',
    },
  },
  {
    id: 95,
    tags: ['SAQ_A_Requirement_12', 'Service_Provider_PCI_DSS_Responsibilities'],
    question:
      'Is information maintained about which PCI DSS requirements are managed by each service provider, and which are managed by the entity?',
    follow_up: {
      Yes: 'Proceed to next section',
      No: 'Document_service_provider_PCI_DSS_responsibilities',
    },
  },
]
